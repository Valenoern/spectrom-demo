;;; demo 2: use spectrom to create something similar to the asekai guide
;;; see demo1_html.lisp for a fuller explanation of spectrom files
;; :folding=explicit: <- this collapses "{{{ ... }}}" in jedit

;; load core spectrom functions / constants / etc
;; and create spectrom-stack to contain cards
(load "spectrom.lisp")
(create-stack)


;;; custom scripting for examples {{{

;;; make kai strain data cards from an abbreviated syntax
(defun k-card (&key (title "") ; {{{
	(num-sector "") (num-index "")
	(align-shift "") (align-phase "") (align-sort "")
	
	(name "")
	(name-pron "")
	(name-mean "")
	(phenoclass "")
	)
	;; &key allows using named arguments like in a constructor
	;; (arg "value") allows setting a default value
	
	;; in the current Real asekai code,
	;; strings that can be localised are in separate swappable cards
	;; but I'm not doing that right now for simplicity.
	
	(create-card
		title  ""
		(L 'num-sector num-sector) (L 'num-index num-index)
		(L 'align-shift align-shift) (L 'align-phase align-phase) (L 'align-sort align-sort)
		(L 'name name)
		(L 'name-pron name-pron)
		(L 'name-mean name-mean)
		(L 'phenoclass phenoclass)
	)
) ; }}}

(defun zerofill (numstring maxsize) "pad a number string to a certain length" ; {{{
	(let (diff (zeroes ""))
		(setq diff (- maxsize (length numstring)))
		;; lisp has a convenient function to make a string of specific length + char
		;; so use it to make exactly n zeroes
		(when (> diff 0)
			(setq zeroes (make-string diff :initial-element #\0))
		)
		(join-str zeroes numstring)
	)) ; }}}

(defun k-number (sector index) "format a kai strain's indexing number" ; {{{
	;; join strings; from spectrom.lisp
	(join-str (zerofill sector 2) "-" (zerofill index 4))
) ; }}}

(defun article (card) "Render a card as HTML" ; {{{
	(let (fields fieldsoup (nn (newline))
			(sector (cfield card 'num-sector)) (index (cfield card 'num-index))
			)
		(if (equal card nil)
			"<div>No card found</div>"
			(progn
				;(setq fieldsoup (debug-cfields card))
				(join-str "<article id=\"" (cfield card "title") "\">" nn
					"<header>" nn
					"  <h2>" (cfield card 'name) "</h2> "
					"<span class=\"number\">" (k-number sector index) "</span>" nn
					"</header>" nn
					"<dl>" nn
					"  <dt>pronunciation</dt> <dd>/" (cfield card 'name-pron) "/</dd>" nn
					"  <dt>aligns</dt> <dd>" (cfield card 'align-shift) " / " (cfield card 'align-phase) " / " (cfield card 'align-sort) "</dd>" nn
					"  <dt>name meaning</dt> <dd>" (cfield card 'name-mean) "</dd>" nn
					"  <dt>visual class</dt> <dd>" (cfield card 'phenoclass) "</dd>" nn
					"</dl>" nn
					"</article>" nn)
			))
	)) ; }}}

(defun entries (cardlist) "print a list of cards as html articles" ; {{{
	(let ((result ""))
		(dolist (card cardlist)
			(setq result (join-str result (newline) (article (get-card card))))
		)
		
		;; output a plain string that could be sent to a file
		(princ result)
	)) ; }}}

;; }}}


;;; examples

(defun test-1 () "create some cards using various syntaxes" ; {{{
	(stack-cards
		(card "k_hokadou" ""
			;; fields are stored on cards as hash keys
			;; to allow putting any arbitrary field on any card
			;; and they get passed this weird way to allow an indefinite number
			(list 'num-sector "1") (list 'num-index "1")
			(list 'align-shift "2") (list 'align-phase "2") (list 'align-sort "dfcf")
			(list 'name "Hokadou")
			(list 'name-pron "ˈhokə.doʊ")
			(list 'name-mean "crackle-roar")
			(list 'phenoclass "archosaur")
		)
		(card "k_valenoern" ""
			;; L is an alias for (list) in spectrom.lisp
			;; it exists, i'm embarrassed to say, just because i still can't
			;; figure out how to create (1 2 3) style lists in common lisp
			(L 'num-sector "1") (L 'num-index "2")
			(L 'align-shift "4") (L 'align-phase "6") (L 'align-sort "dpep")
			(L 'name "Valenoern")
			(L 'name-pron "ˈvælɛnʔɝn")
			(L 'name-mean "valley lion-eagle")
			(L 'phenoclass "mountain cat")
		)
		;; here the basic way of creating fields is abstracted to be shorter
		;; you could make the 'short' fields anything and get the same card
		;; which will be important for overlaying modules of cards on each other
		(k-card :title "k_spelaea"
			:num-sector "3" :num-index "1"
			:align-shift "3" :align-phase "3" :align-sort "dpcp"
			
			:name "Sphelaea"
			:name-pron "spɛˈleɪə"
			:name-mean "sphere lion"
			:phenoclass "lion"
		)
	)
	
	(entries (L "k_hokadou" "k_valenoern" "k_spelaea"))
	
	#| output: {{{
	
	<article id="k_hokadou">
	<header>
	  <h2>Hokadou</h2> <span class="number">1-1</span>
	</header>
	<dl>
	  <dt>pronunciation</dt> <dd>/ˈhokə.doʊ/</dd>
	  <dt>aligns</dt> <dd>2 / 2 / dfcf</dd>
	  <dt>name meaning</dt> <dd>crackle-roar</dd>
	  <dt>visual class</dt> <dd>archosaur</dd>
	</dl>
	</article>
	
	<article id="k_valenoern">
	<header>
	  <h2>Valenoern</h2> <span class="number">1-2</span>
	</header>
	<dl>
	  <dt>pronunciation</dt> <dd>/ˈvælɛnʔɝn/</dd>
	  <dt>aligns</dt> <dd>4 / 6 / dpep</dd>
	  <dt>name meaning</dt> <dd>valley lion-eagle</dd>
	  <dt>visual class</dt> <dd>mountain cat</dd>
	</dl>
	</article>
	
	<article id="k_spelaea">
	<header>
	  <h2>Sphelaea</h2> <span class="number">3-1</span>
	</header>
	<dl>
	  <dt>pronunciation</dt> <dd>/spɛˈleɪə/</dd>
	  <dt>aligns</dt> <dd>3 / 3 / dpcp</dd>
	  <dt>name meaning</dt> <dd>sphere lion</dd>
	  <dt>visual class</dt> <dd>lion</dd>
	</dl>
	</article>
	}}} |#
) ; }}}

;;; TODO: create functions that
;;; - generate multiple l10n cards
;;; - allow an appropriate amount of composing as in kai strain hjson plugins

(test-1)