;;; demo of using lisp to do things similar to asekai tw->guide exporter {{{
;;; :folding=explicit:

(defstruct spectrom-stack
	(cards (make-hash-table :test 'equal))
	(cardnames nil)
)

(defstruct card
	(title "")
	(body "")
	(fields (make-hash-table))
	(fieldnames nil)
)

(defvar spectrom-stack)

(defun newline () "
")

(defun alias (new-name prev-name)
	(setf (symbol-function new-name) (symbol-function prev-name)))

(defun join-str (&rest strings)
	(let ((result ""))
		(dolist (str strings)
			(setq result (concatenate 'string result str)))
		result
	))

(alias 'l 'list)

;; TODO: create 'lines' function

;; }}}

;;; card functions {

;; create card
;; extra arguments (&rest) will become arbitrary fields
(defun create-card (title body &rest fields) ; {{{
	(let (card fieldnames fieldvalues fieldname fieldvalue)
		;; create hash table to hold card fields
		(setq fieldvalues (make-hash-table))
		;(setq fieldnames nil)
		
		(dolist (fieldtuple fields)
			;; fields are expressed (field value)
			;; so, extract those two values
			(setq fieldname (nth 0 fieldtuple))
			(setq fieldvalue (nth 1 fieldtuple))
			;; convert the field to a hash table entry
			(setf (gethash fieldname fieldvalues) fieldvalue)
			;; store field in list
			(setq fieldnames (cons fieldname fieldnames))
		)
		
		(setq card
			(make-card :title title :body body
				:fields fieldvalues :fieldnames fieldnames)
		)
		
		card))
(alias 'card 'create-card) ; }}}

;; retrieve field from card
(defun cfield (card field) ; {{{
	(let ((fields (card-fields card)) result)
		(if (not (or (equal field "title") (equal field "body")))
			;; then
			(setq result (gethash field fields nil))
			;; else
			(progn
				(when (equal field "title")
					(setq result (card-title card)))
				(when (equal field "body")
					(setq result (card-body card)))
			)
		)
		result)) ; }}}

;;; construct empty stack
(defun create-stack ()
	(let ((cards (make-hash-table :test 'equal)) (cardnames nil))
		;(setq spectrom-stack (make-spectrom-stack :cards cards :cardnames cardnames))
		(setq spectrom-stack (make-spectrom-stack))
	))

;;; add card(s) into global card stack
(defun stack-cards (&rest cards) ; {{{
	(let (cardname
			(cardnames (spectrom-stack-cardnames spectrom-stack))
			(cardtable (spectrom-stack-cards spectrom-stack))
			)
		(dolist (card cards)
			(setq cardname (cfield card "title"))
			(setf (gethash cardname cardtable) card)
			(setf cardnames (cons cardname cardnames))
		)
		;; when you use setf it doesn't seem you can store a gethash
		;; or "getter" result; you must use the functions
		(setf (spectrom-stack-cardnames spectrom-stack) cardnames)
		(setf (spectrom-stack-cards spectrom-stack) cardtable)
	)) ; }}}

(defun get-card (cardname) ; {{{
	(gethash cardname (spectrom-stack-cards spectrom-stack) nil)
) ; }}}

;; debug cards / stack to terminal {{{
(defun debug-cfields (card) ; {{{
	(let (title body result (nn (newline)) (fields (card-fieldnames card)) fieldvalue)
		(setq title (cfield card "title"))
		(setq body (cfield card "body"))
		
		;; add title and body to debug output
		(setq result (join-str "title: " title ""))
		(setq result (join-str result nn "body: " body ""))
		
		(dolist (fieldname fields)
			(setq fieldvalue (cfield card fieldname))
			(setq fieldname (symbol-name fieldname))
			(setq result 
				(join-str result nn fieldname ": " fieldvalue))
		)
		result
	)) ; }}}

(defun debug-stack () ; {{{
	(let ((result "") (nn (newline)) card
			(cardnames (spectrom-stack-cardnames spectrom-stack))
		)
		
		(write "debug card stack:")
		
		(dolist (cardname cardnames)
			(setq card (gethash cardname (spectrom-stack-cards spectrom-stack)))
			(write (join-str nn nn "- " cardname ": " nn (debug-cfields card)))
		)
		
		(terpri)
	)) ; }}}
;; }}}

