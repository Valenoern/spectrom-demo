;;; demo 1: creating "cards" and printing them
;; :folding=explicit: <- this collapses "{{{ ... }}}" in jedit

;;; purpose/concept of spectrom:
;; you load the spectrom library,
;; then use a lisp file to create and transform "cards" in various ways
;; this file can thus contain a simple data set AND instructions to export it,
;; like a more lightweight version of hacking a tiddlywiki to export datafiles.

;;; load core spectrom functions / constants / etc
;;; and create spectrom-stack to contain cards
(load "spectrom.lisp")
(create-stack)

;;; custom scripting section

(defun card-div (card) "Render a card as HTML" ; {{{
	(let (fields fieldsoup (nn (newline)))
		(if (equal card nil)
			"<div>No card found</div>"
			(progn
				(setq fieldsoup (debug-cfields card))
				(concatenate 'string "<div>"
					"<h2>" (cfield card "title") "</h2>" nn
					"<p>" (cfield card "body") "</p>" nn
					"<template>" nn
					fieldsoup nn
					"</template></div>" nn)
			))
	)) ; }}}

;;; tests
;; in this file cards are "conditionally" created when running specific functions
;; that's specifically to separate tests from each other
;; and might or might not be the typical way to do it

(defun test-1 () "create an example card and print it as a div" ; {{{
	(let (acard)
		(setq acard
			(create-card "Test card" "body text" (list 'arbitrary "alice"))
		)
		;; could also be expressed let ((acard (create-card ....)))
		;; this just seemed more readable
		
		;; (write) can print any value, but you need (terpri) to print an \n
		(write (card-div acard))
		(terpri)
		
		#| output:
		"<div><h2>Test card</h2>
		<p>body text</p>
		<template>
		title: Test card
		body: body text
		ARBITRARY: alice
		</template></div>
		"
		|#
	)) ; }}}

(defun test-2 () "create two cards on the stack, then retrieve & print them" ; {{{
	(let (cards (result ""))
		(stack-cards
			(card "Card 1" "asdf" (list 'arbitrary "alice"))
			(card "Card 2" "wer" (list 'arbitrary "bob"))
		)
		
		(setq cards (list "Card 1" "Card 2"))
		
		(dolist (card cards)
			(setq result (join-str (newline) (card-div (get-card card)) result))
		)
		
		;; princ prints a string without quotes, escapes, etc
		(princ result)
		
		#| output:
		
		<div><h2>Card 2</h2>
		<p>wer</p>
		<template>
		title: Card 2
		body: wer
		ARBITRARY: bob
		</template></div>
		
		<div><h2>Card 1</h2>
		<p>asdf</p>
		<template>
		title: Card 1
		body: asdf
		ARBITRARY: alice
		</template></div>
		|#
	)) ; }}}


(test-2)