= PURPOSE

asekai currently uses a tiddlywiki to edit and export game data.
but after the very early stages when the data format started
to crystalise, this was proving to be needlessly hard to maintain
and complex.

tiddlywiki's plugin system required maintaining a JS plugin to support
entering elaborate plugins in a suitably simple hjson format,
and it was just kind of a pain.
at the same time i was messing around with exporting blog entries etc
with tiddlywiki, and its love of inserting unwanted <p> tags had driven
me straight out of using its widget syntax to making templates in JS.

neither of these things was very good, compared to when I started messing
with lisp and found it was much better for both
- "booting up" + exporting strings quickly
- arbitrarily abstracting code into shorter forms

thus I set out to use lisp to try to reimplement only the "good parts" of
tiddlywiki (regarding exporting a datafile or html page, anyway),
and create "a simpler TW-like thing one can entirely write in a text editor".

(I suppose that's just called "a scripting library",
but it also draws on tiddlywiki's concept of a scriptable data document.)

it is by far not done yet.


= EXAMPLES

to suit the incremental, rather slow way this is being developed,
I've decided to lay out the examples in a format weirdly similar to a
programming tutorial book — so they're best read in order.


= LICENSE

the finished spectrom will be GPL3 so for consistency this one is too.
